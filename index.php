<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" > 
  <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />-->
  <title>Home</title>
  <meta name="description" content="PHP skill assignment" >
  <link rel="stylesheet" type="text/css" href="css/style.css" >
 
  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="js/libs/modernizr-2.5.3.min.js" type="text/javascript" ></script>
</head>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div class="pre-header clearfix"> 
<div class="logo"><h1 class="logo_h">MG</h1></div>
<div class="header" > 
<div class="Nav">
  <!--Including Navigation File -->
<?php 
    include('mainNav.php');
?>
</div>
</div>
</div>


<div id="main" class="body-content">

		<!--start responsive code -->
      <div style=" margin:0 auto; position:relative;overflow:hidden;"> <!-- define slider container width (by strict force) -->
        <div class="demoholder">
          <div class="bggradient"></div>
          <!-- <div class="kbpanel-bannercontainer centerme light"> light theme -->
          <div class="kbpanel-bannercontainer centerme dark">
            <ul>
              <!-- Slide 1 -->
              <li data-transition="fade" data-startalign="right,bottom" data-zoom="in" data-zoomfact="3" data-endAlign="center,top" data-panduration="12" data-colortransition="4"><img alt="" src="http://lorempixel.com/890/390/sports/8/" data-bw="http://lorempixel.com/890/390/sports/9/" data-thumb="http://lorempixel.com/890/390/nightlife/1/">
                <div class="creative_layer">
                  <div class="cp-left faderight" >
                    <p class="cp-title">Ken Burns Panel Slider</p>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                    <p>Lorem ipsum dolor sit amet, consectetuer <br />
                      adipiscing elit. Aenean commodo ligula <br />
                      eget dolor. Aenean massa.  Donec pede justo,<br />
                      arcu. In enim justo,<br />
                    </p>
                    <a style="margin-top:10px" class="kb-button" href="#">See More</a>
                    <div class="clear"></div>
                  </div>
                </div>
              </li>
              
              <!-- Slide 2 -->
              <li data-transition="slide" data-startalign="right,top" data-zoom="out" data-zoomfact="1.5" data-endAlign="left,bottom" data-panduration="8" data-colortransition="4"><img alt="" src="http://lorempixel.com/890/390/sports/3/" data-bw="http://lorempixel.com/890/390/sports/4/" data-thumb="http://lorempixel.com/890/390/abstract/2/">
                <div class="video_kenburner">
                  <div class="video_kenburn_wrap">
                    <div class="video_video">
                      <iframe class="video_clip" src="http://player.vimeo.com/video/4749536?title=0&amp;byline=0&amp;portrait=0"></iframe>
                    </div>
                    <div class="video_details">
                      <p class="cp-title">Vimeo Video</p>
                      <p>Watch sample embedded Vimeo video.<br />
                      </p>
                      <a style="margin-top:10px" class="kb-button" href="#">See More</a>
                      <div class="clear"></div>
                    </div>
                    <div class="close"></div>
                  </div>
                </div>
                <div class="creative_layer ">
                  <div class="cp-right fade">
                    <p class="cp-title">Vimeo Support</p>
                    <p>This slider supports embedded content.<br />
                      Click the play button to play a sample video.<br />
                    </p>
                    <a style="margin-top:10px" class="kb-button" href="#">See More</a>
                    <div class="clear"></div>
                  </div>
                </div>
              </li>
              
              
              <!-- Slide 4 -->
              <li data-transition="fade" data-startalign="right,top" data-zoom="out" data-zoomfact="1.2" data-endAlign="left,bottom" data-panduration="11" data-colortransition="4"><img alt="" src="http://lorempixel.com/890/390/city/1/" data-bw="http://lorempixel.com/890/390/city/2/" data-thumb="http://pimg.co/p/890x390/333/fff/img.png">
                <div class="creative_layer ">
                  <div class="cp-top fadedown" >
                    <p class="cp-title">Your Portfolio, Revealed.</p>
                    <p>Showcase your work and bring still images to life!</p>
                  </div>
                </div>
              </li>
              

            </ul>
          </div>
          <!-- /kbpanel-bannercontainer --> 
          <!-- Banner Shadow Effect -->
          <div class="bannershadow centerme"><img  alt="slider element" src="img/bannershadow.png"></div>
        </div>
        <!-- /demoholder --> 
        
      </div>
     <!--end responsive code -->
	 

<h1>Welcome !!!</h1>


<?php 
    include("home.par2.txt");
 ?>


<?php 
    include("home.par1.txt");
?>


</div>

<div class="pre-footer footers">
<div class="footer clear">
<?php 
    include("footerNav.php");
?>
</div>

</div>

    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript" ></script>
  <!--<script type="text/javascript" >
  window.jQuery || document.write('<script  src="js/libs/jquery-1.7.1.min.js" type="text/javascript"></script>')
  </script>
  -->
  <script  src="js/plugins.js" type="text/javascript" ></script>
  <script  src="js/script.js" type="text/javascript" ></script>
  <!-- end scripts -->  
  <!-- scripts concatenated and minified via build script -->
  <!-- slides START -->
  <!-- start responsive script -->
  <!-- DC Javascript Library -->
  <script type="text/javascript" src="js/jquery.easing.js"></script><!-- jQuery Easing (do not call twice on same page) -->
  <!-- DC Ken Burns Panel Slider JS -->
  <script type="text/javascript" src="js/jquery.waitforimages.js"></script>
  <script type="text/javascript" src="js/jquery.cssAnimate.mini.js"></script>
  <script type="text/javascript" src="js/jquery.templateaccess.kenburns-panel.min.js"></script>
  <script type="text/javascript">
  $(function() {   
    $('.kbpanel-bannercontainer').kenburn({
 
        thumbWidth:70, // thumbnail width
        thumbHeight:40, // thumbnail height
         
        thumbAmount:4,  // number of thumbnails to show
        thumbStyle:"both",   // thumb, bullet, none, both
        thumbVideoIcon:"on", // show a video icon for video content: off, on
         
        thumbVertical:"bottom",
        thumbHorizontal:"center",                          
        thumbXOffset:0,
        thumbYOffset:40,
        bulletXOffset:0,
        bulletYOffset:-16,
        hideThumbs:"on",
        touchenabled:'on',  // allow touch swipe (suitable for mobile devices): on, off
        pauseOnRollOverThumbs:'off', // pause slider when mouse over thumbnail
        pauseOnRollOverMain:'on', // pause slider when mouse over slider
        preloadedSlides:2, // number of slides to preload during startup
         
        timer:5, // time before next slide (5 = 5 seconds)
         
        debug:"off",                       
         
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //                                               Google Fonts !!                                             //
        // local GoogleFont JS from your server: http://www.yourdomain.com/kb-plugin/js/jquery.googlefonts.js        //
        // GoogleFonts from Original Source: http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js or https:... //
        //          PT+Sans+Narrow:400,700                                                                          //
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        googleFonts:'Oswald',
        googleFontJS:'js/jquery.googlefonts.js'
    });
         
         
    $('.tnone').click(function() {
        $('.tbull').removeClass('selected');
        $('.tthumb').removeClass('selected');
        $('.tnone').addClass('selected');
        $('.tauto').removeClass('selected');
        $('.kenburn_thumb_container').css({'visibility':'hidden'});
        $('.thumbbuttons').css({'visibility':'hidden'});       
    });
     
    $('.tthumb').click(function() {
        $('.tbull').removeClass('selected');
        $('.tauto').removeClass('selected');
        $('.tthumb').addClass('selected');
        $('.tnone').removeClass('selected');
        $('.kenburn_thumb_container').css({'visibility':'visible'});
        $('.thumbbuttons').css({'visibility':'hidden'});
        $('body').addClass('tp_showthumbsalways');
        $('.kenburn_thumb_container').animate({'opacity':1},{duration:300,queue:false});
         
    });
     
    $('.tauto').click(function() {
        $('.tauto').addClass('selected');
        $('.tthumb').removeClass('selected');
        $('.tnone').removeClass('selected');
        $('.tbull').removeClass('selected');
        $('body').removeClass('tp_showthumbsalways');
        $('.kenburn_thumb_container').css({'visibility':'visible'});
        $('.thumbbuttons').css({'visibility':'hidden'});
        setTimeout(function() {
            $('.kenburn_thumb_container').animate({'opacity':0},{duration:300,queue:false});
        },100);
         
    });
     
    $('.tbull').click(function() {
        $('.tbull').addClass('selected');
        $('.tauto').removeClass('selected');
        $('.tthumb').removeClass('selected');
        $('.tnone').removeClass('selected');
        $('.kenburn_thumb_container').css({'visibility':'hidden'});
        $('.thumbbuttons').css({'visibility':'visible'});       
    });
     
    $('body').addClass('tp_showthumbsalways');
    $('.tthumb').click();
         
});
</script>
  <!-- DC Ken Burns Panel Slider Settings -->
  <!-- end responsive script -->
  <!-- slides END -->
  <script type="text/javascript" src="js/jquery.tinycarousel.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#slider1').tinycarousel(); 
    });
  </script>
<!-- END slider -->
</body>
</html>