
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<head>
  <title>News Page</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" > 
  <meta name="description" content="PHP skill assignment" >
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="js/libs/modernizr-2.5.3.min.js" type="text/javascript" ></script>
</head>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div class="pre-header"> 
<div class="logo"><h1 class="logo_h">MG</h1></div>
<div class="header clearfix" > 
<div class="Nav">
<?php 
    include('mainNav.php');
?>
</div>
</div>
</div>


<div id="main" class="body-content">
<!-- Main Heading -->
<h1 class="newsHeading">News</h1>

<?php
 
  //Establish connection with the server
   $con = mysql_connect('54.243.57.9','a.sidlinskiy', 'uucyc#777');
  
   
   //Choosing our Database
   mysql_select_db('Alex_Sidlinskiy_Final_Project', $con);

  //here we query the database
  $query = mysql_query("SELECT * FROM News");
     
  //while there's records in in News Table Do this
    while($row = mysql_fetch_array($query))
    {
        //Here we building the string $newFeed that will news data displayed in containers with spans 
	      $newsFeed  =  "<div class='news'>\n" . "" . "<div class='news_date'>" . $row['date'] ."</div>\n";
	      $newsFeed .=  "<div class='news_feed'>". $row['storytext']."</div>\n" . "</div>". "\n";
	      $newsFeed .=  "<div class='clearfix'></div>\n"; 
	      
       //Displaying news page
        echo $newsFeed;
     	  
    }
  //close mysql                      
  mysql_close($con);
?>

<div class="clearfix"></div>
</div><!-- Close main content -->

<!-- Footer Area content starts -->
<div class="pre-footer">
<div class="footer">
<?php 
    include("footerNav.php");
?>
</div>
</div>
<!-- End of Footer content -->


  <!-- JavaScript at the bottom for fast page loading -->
  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript" ></script>
  <!--<script type="text/javascript" >
  window.jQuery || document.write('<script  src="js/libs/jquery-1.7.1.min.js" type="text/javascript"></script>')
  </script>
  -->
  <!-- scripts concatenated and minified via build script -->
<script  src="js/plugins.js" type="text/javascript" ></script>
<script  src="js/script.js" type="text/javascript" ></script>
  <!-- end scripts -->

</body>
</html>
