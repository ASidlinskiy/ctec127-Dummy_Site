<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
  <title>Order Page </title>
  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="PHP skill assignment" >
  <link href="css/style.css" rel="stylesheet">
   <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  	<script src="js/libs/modernizr-2.5.3.min.js" type="text/javascript" ></script>
	

</head>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div class="pre-header"> 
	<div class="logo"><h1 class="logo_h">MG</h1></div>
		<div class="header clearfix" > 
			<div class="Nav">
				<?php 
				    include('mainNav.php');
				?>
			</div>
		</div>
	</div>
<div id="main" class="body-content">
<!-- Start of Form -->
<form  name="f" class="order " method="post" action="process_form.php"> 
	<fieldset class="fset">
		<legend class="lgnd">Customer / Order Details</legend>

			<label for="FirstName" class="labels fn">First Name</label> 
			<input class="fn firstname o" placeholder="e.g.,&nbsp;James" name="FirstName" id="FirstName" required="required" pattern="[A-Za-z]+"/> 

			<label for="LastName" class="labels ln">Last Name</label> 
			<input class="ln lastnamet o" placeholder="e.g.,&nbsp; Jordan" name="LastName" id="LastName" required="required" pattern="[A-Za-z]+" />

			<label for="address1" class="labels ad1">Address 1:</label> 
			<input class="ad1 address1 o" placeholder="e.g.,&nbsp;12345 15th Ave NE" name="Address1" id="address1" required="required" pattern="[A-Za-z0-9]+" /> 

			<label for="address2" class="labels ad2">Address 2:</label> 
			<input class="ad2 address2 o" placeholder="e.g.,&nbsp;A310" id="address2" name="Address2" pattern="[A-Za-z0-9]+" />

			<label for="City" class="labels c">City</label> 
			<input class="c city o" name="City" placeholder="e.g.,&nbsp;Los Angeles" id="City" required="required" pattern="[A-Za-z]+" />

			<label for="State" class="labels st">State</label> 
			<div id="fix_width">
			<select class="state st " name="State" id="State" required="required">
				<option value="">Select State</option>
				<option value="AL">Alabama</option>
				<option value="AK">Alaska</option>
				<option value="AZ">Arizona</option>
				<option value="AR">Arkansas</option>
				<option value="CA">California</option>
				<option value="CO">Colorado</option>
				<option value="CT">Connecticut</option>
				<option value="DE">Delaware</option>
				<option value="DC">District of Columbia</option>
				<option value="FL">Florida</option>
				<option value="GA">Georgia</option>
				<option value="HI">Hawaii</option>
				<option value="ID">Idaho</option>
				<option value="IL">Illinois</option>
				<option value="IN">Indiana</option>
				<option value="IA">Iowa</option>
				<option value="KS">Kansas</option>
				<option value="KY">Kentucky</option>
				<option value="LA">Louisiana</option>
				<option value="ME">Maine</option>
				<option value="MD">Maryland</option>
				<option value="MA">Massachusetts</option>
				<option value="MI">Michigan</option>
				<option value="MN">Minnesota</option>
				<option value="MS">Mississippi</option>
				<option value="MO">Missouri</option>
				<option value="MT">Montana</option>
				<option value="NE">Nebraska</option>
				<option value="NV">Nevada</option>
				<option value="NH">New Hampshire</option>
				<option value="NJ">New Jersey</option>
				<option value="NM">New Mexico</option>
				<option value="NY">New York</option>
				<option value="NC">North Carolina</option>
				<option value="ND">North Dakota</option>
				<option value="OH">Ohio</option>
				<option value="OK">Oklahoma</option>
				<option value="OR">Oregon</option>
				<option value="PA">Pennsylvania</option>
				<option value="RI">Rhode Island</option>
				<option value="SC">South Carolina</option>
				<option value="SD">South Dakota</option>
				<option value="TN">Tennessee</option>
				<option value="TX">Texas</option>
				<option value="UT">Utah</option>
				<option value="VT">Vermont</option>
				<option value="VA">Virginia</option>
				<option value="WA">Washington</option>
				<option value="WV">West Virginia</option>
				<option value="WI">Wisconsin</option>
				<option value="WY">Wyoming</option>
			</select>
			</div><br />
			
			<label for="ZipCode" class="zc labels">Zip-Code</label>
			<input class="zc zip_code o" name="Zip_Code" placeholder="e.g.,&nbsp;68805" id="ZipCode" required="required" pattern="[0-9]+"/> <br />
			<label for="products[0]" class="p labels ">Products:</label>
			<input type="checkbox" class="ch pr_c" name="products[]"  id="products[0]" value='14_Mac_Book_Pro' style="margin-left:65px; top:10px"/>14" Mac Book Pro<br>
			<input type="checkbox" name="products[]" id="products[1]" class="ch pr_c" value="Samsung_Galaxy_Tab_2" />Samsung Galaxy Tab 2<br ><br>
			<input type="checkbox" class="ch pr_c" name="products[]" id="products[2]" value="I_phone_Headphones" />I-phone Headphones<br><br>
			<input type="checkbox" name="products[]" id="products[3]" class="ch pr_c" value="Sony_Headphones" />Sony Headphones<br><br>
			<input type="checkbox" class="ch pr_c" name="products[]" id="products[4]" value="HTC_G2 " />HTC G2
		</fieldset><button type="submit" class="submit" value="Submit" >Submit</button>
	</form>
<!-- End of Form -->
</div>  <!-- End Body Content Div -->

<div class="pre-footer">
<div class="footer ">
<?php 
    include("footerNav.php");
?>
</div>

</div>


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript" ></script>
  <!--<script type="text/javascript" >
  window.jQuery || document.write('<script  src="js/libs/jquery-1.7.1.min.js" type="text/javascript"></script>')
  </script>
  -->
  <!-- scripts concatenated and minified via build script -->
<script  src="js/plugins.js" type="text/javascript" ></script>
<script  src="js/script.js" type="text/javascript" ></script>
  <!-- end scripts -->

</body>
</html>

