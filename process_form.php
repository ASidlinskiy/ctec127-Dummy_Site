
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <meta name="description" content="PHP skill assignment" />
  
   <link rel="stylesheet" type="text/css" href="css/cssreset.css" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="stylesheet" type="text/css" href="css/kenburns-panel-settings.css" />
  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="js/libs/modernizr-2.5.3.min.js" type="text/javascript" ></script>
<title>Process Form</title>
</head>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div class="pre-header"> 
<div class="logo"><h1 class="logo_h">MG</h1></div>
<div class="header clearfix" > 
<div class="Nav">
<?php 
    include('mainNav.php');
?>
</div>
</div>
</div>
<div id="main" class="body-content">
<!--main Heading --> 
<h2 class='mtext'>Here's what you have entered:</h2>

<!-- PHP file processes table -->
<?php 
//initializing $field_count variable 
$field_count = 0;

//Start bulding table
echo "<table > ";
// header of the table
echo "<tr><th >Field</th><th>Value</th></tr>";

//For each  field set to $_POST display field name($name) and its value($field)
foreach($_POST as $name => $field)
{
  //increment field counter as it loops through each field  
	$field_count++;

    //First row of the table
	echo "<tr class='col1'><td>" . $name . "</td>";

	 // if it is an array with filds proceed 
  //if not display it empty 
		if(is_array($field))
		{   
      //assigning number of fields in array to $count variable
			$count = count($field);
			echo "<td class='col2'><ul>";
			
				//here we check if checkboxes are checked off 
				for($i=0; $i<$count; $i++)
				{
					//display each check box value
          echo "<li>". $field[$i] . "</li>";
				}

      
			echo "</ul></td>";
		}
		else
		{
			echo"<td class='col2'>$field</td>";
		}
		
	echo "<tr>\n";
}	 

echo "	</table> \n ";


?>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</div>

<div class="pre-footer">
<div class="footer clearfix">
<?php 
    include("footerNav.php");
?>
</div>

</div>


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript" ></script>
  <!--<script type="text/javascript" >
  window.jQuery || document.write('<script  src="js/libs/jquery-1.7.1.min.js" type="text/javascript"></script>')
  </script>
  -->
  <!-- scripts concatenated and minified via build script -->
<script  src="js/plugins.js" type="text/javascript" ></script>
<script  src="js/script.js" type="text/javascript" ></script>
  <!-- end scripts -->

