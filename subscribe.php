<form action="process_form" class="subscribe-container" method="post">
<fieldset>
<legend>News Letter</legend>
<label for="Name" class="sub_fn labelname">Name</label>
<input class="sub_fn subscribe_name" placeholder="e.g., James Jordan" type="text" id="Name" name="Name" 
required="required" pattern="\s*[A-z]+" /><br />

<label for="Email" class="sub_fe labelemail">Email</label>
<input class="sub_fe subscribe_email"  placeholder="e.g., james@gmail.com" id="Email" name="E-mail" type="text" required="required" pattern="^\s*\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}\s*$" />
</fieldset>
<button class="f_submit" type="submit">Sign Up</button>
</form>