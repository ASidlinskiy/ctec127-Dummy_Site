<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Home</title>
  <meta name="description" content="PHP skill assignment" />

  <link rel="stylesheet" type="text/css" href="css/cssreset.css" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="stylesheet" type="text/css" href="css/kenburns-panel-settings.css" />
  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="js/libs/modernizr-2.5.3.min.js" type="text/javascript" ></script>
<title>Form</title>
<style type="text/css">

table
{
border-collapse:collapse;
}
table,th, td
{
border: 1px solid black;
}
{
width:100%;
}
th
{
height:50px;

</style>
</head>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div class="pre-header"> 
<div class="logo"><h1 class="logo_h">MG</h1></div>
<div class="header clearfix" > 
<div class="Nav">
<?php 
    include('mainNav.php');
?>
</div>
</div>
</div>


<div id="main" class="body-content">
 <!--Main heading-->
<h1>Form</h1>
<form id="f" name="f" method="post" action="http://ctec.clark.edu/~belgort/php/unit7/forms/formProcessor.php"> 
<label >First Name</label> <input id="username" name="First Name" /> <br />
<label >Last Name</label> <input id="userlast" name="Last Name" /> <br />
<label >Favorite Color</label> <input id="email" name="Favorite Color"  /><br />
<label >School Attended</label> <input id="email" name="School Attended"  /> <br />
<label >Favorite Car</label> <input id="email" name="Favorite Car"  />  <br />
<label >Favorite Meal</label> <input id="email" name="Favorite Meal"  /> <br />
<label >School GPA:</label> <input id="email" name="School GPA:"  /><br />
<label >Phone</label> <input id="email" name="Phone"  /> <br />
<label >Email</label> <input id="email" name="Email"  /> <br />
<label >Address 1:</label> <input id="email" name="Address 1:"  />  <br />
<label >Address 2:</label> <input id="email" name="Address 2:"  /><br />
<label >City:</label> <input id="city" name="City:"  /><br />
<label >State</label> <input id="State" name="State"  /><br />     <br />
<label for="coffee[]">Favorite Coffee</label>
<input type="checkbox" name="coffee[]" value="Seattles Best Coffee" />Seattles Best Coffee <input type="checkbox" name="coffee[]" value="Starbucks" />Starbucksbr />
<input type="reset"  value="Clear Form" /><input type="submit" name="Submit" value="Submit" />
</form>
<?php 


?>


</div>
<div class="clearfix"></div>
<div class="pre-footer">
<div class="footer clearfix">
<?php 
    include("footerNav.php");
?>
</div>

</div>


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript" ></script>
  <!--<script type="text/javascript" >
  window.jQuery || document.write('<script  src="js/libs/jquery-1.7.1.min.js" type="text/javascript"></script>')
  </script>
  -->
  <!-- scripts concatenated and minified via build script -->
<script  src="js/plugins.js" type="text/javascript" ></script>
<script  src="js/script.js" type="text/javascript" ></script>
  <!-- end scripts -->

</body>
</html>

