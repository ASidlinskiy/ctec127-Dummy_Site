<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<head>
  <title>Product Page</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" > 
  <meta name="description" content="PHP skill assignment" >
  <link rel="stylesheet" type="text/css" href="css/style.css" >
  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="js/libs/modernizr-2.5.3.min.js" type="text/javascript" ></script>
</head>
<body>
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

<div class="pre-header"> 
<div class="logo"><h1 class="logo_h">MG</h1></div>
<div class="header clearfix" > 
<div class="Nav">
<?php 
    include('mainNav.php');
?>
</div>
</div>
</div>


<div id="main" class="body-content">

<h1>Products</h1>

<?php

#file we want to read
$filetoread = "products.txt";

#get handle to the file
$file_handle = fopen($filetoread, "r");

#loop through the file until you're done  
while(!feof($file_handle))
{
	
	#read line by line
	$line = fgets($file_handle);
	
	#show the contents 
	for($i=0; $i<count($line); $i++)
		{
		  
      //here we split lines on /*/  and assign results to products variable
      $products = explode('/*/', $line);
		 //here we contstruct the semantics of a page with html tags
		for($i=0; $i<count($products)-2; $i++)
			{
				$p_row  = "<div class='p_row clearfix'>"."\n". "<span class='p_product'>" . $products[0] . "</span>\n";
				$p_row .= "<span class='p_price'>" . $products[1] . "</span>" ." \n". "<span class='p_image'>" . $products[2] . "</span>";  
				$p_row .= "</div>\n";
				
				echo $p_row;
			}
		  
		}

}
//close the file we're done reading at this point
fclose($file_handle);

?>

<div class="clearfix"></div>

</div>
<div class="pre-footer">
<div class="footer">

<?php 
    include("footerNav.php");
?>
</div>
</div>


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript" ></script>
  <!--<script type="text/javascript" >
  window.jQuery || document.write('<script  src="js/libs/jquery-1.7.1.min.js" type="text/javascript"></script>')
  </script>
  -->
  <!-- scripts concatenated and minified via build script -->
<script  src="js/plugins.js" type="text/javascript" ></script>
<script  src="js/script.js" type="text/javascript" ></script>
  <!-- end scripts -->

</body>
</html>